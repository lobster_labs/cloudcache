#!/usr/bin/env python2.7

import argparse
import cherrypy
from cloudcache.api import (
    DropboxAPI,
)
from cloudcache.updown import DownloadService, UploadService
from cloudcache.providers import DropboxProvider
import warnings

warnings.filterwarnings('ignore')


def parse_args():
    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        '-s', '--server-config',
        default='server.conf',
        help="Server config file."
    )
    return argparser.parse_args()


def run():
    args = parse_args()
    config = cherrypy.config
    config.update(args.server_config)

    # Init services
    download_service = DownloadService.from_config('downloads', config)
    download_service.subscribe()

    upload_service = UploadService.from_config('uploads', config)
    upload_service.subscribe()

    # Providers should start after rest of the services are started and stop
    # before rest of the services are stopped.
    provider_priorities = {'start': 60, 'stop': 40}

    dbx_provider = DropboxProvider.from_config(
        'dropbox',
        config,
        channel_notify='notify',
        channel_download=download_service.channel_enqueue,
        channel_upload=upload_service.channel_enqueue
    )
    dbx_provider.subscribe(
        priorities=provider_priorities
    )

    # Mount Web APIs
    cherrypy.tree.mount(DropboxAPI(dbx_provider), '/')

    # Start server
    cherrypy.engine.start()
    cherrypy.engine.block()

if __name__ == '__main__':
    run()
