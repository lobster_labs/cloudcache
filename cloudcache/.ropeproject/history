�]q(]q(U	ChangeSetqX+   Renaming <errrr_handler> to <error_handler>q]qUChangeContentsqUapi.pyqX�  #!/usr/bin/env python2.7

"""
Web APIs.
"""


from __future__ import absolute_import

import cherrypy
import cherrypy.lib
import cherrypy.lib.static
import cherrypy.process.plugins
import contextlib
import functools
from cloudcache.util import json_encoder


def large_upload():
    cherrypy.response.timeout = 3600
    cherrypy.request.process_request_body = False

cherrypy.tools.large_upload = cherrypy.Tool('before_request_body', large_upload)


def error_handler(f):
    """Translate exceptions to HTTP response with appropriate status code
    and JSON-serialized response body.

    Exceptions can specify HTTP status code via http_status_code attribute, with
    HTTP status 500 (Internal Server Error) as the default.
    """
    @functools.wraps(f)
    def wrapped(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as err:
            status_code = getattr(err, 'http_status_code', 500)
            cherrypy.log('An exception occured', traceback=True)

            response = cherrypy.serving.response
            response.status = status_code
            response.headers['Content-Type'] = 'application/json'

            return json_encoder.encode(
                {
                    'error': {
                        'type': type(err).__name__,
                        'args': err.args
                    }
                }
            )
    return wrapped


def json_handler(*args, **kwargs):
    """CherryPy request handler using custom JSON encoder."""
    value = cherrypy.serving.request._json_inner_handler(*args, **kwargs)
    return json_encoder.iterencode(value)


def str_to_bool(o):
    return str(o).lower() in ('true', 'yes', 'aye', 'yup', 'totally', 'yessir')


class DropboxAPI(object):
    """Web API for dropbox provider."""

    def __init__(self, dbx_provider=None):
        self.dbx_provider = dbx_provider

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @cherrypy.tools.json_out(handler=json_handler)
    @error_handler
    def status(self):
        """Return storage status from storage provider."""
        return self.dbx_provider.status()

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @error_handler
    def fetch(self, path, ignore_cache='false', chunk_size=64*1024):
        """Request a file from storage."""
        cherrypy.serving.response.stream = True

        ignore_cache = str_to_bool(ignore_cache)
        chunk_size = int(chunk_size)

        if ignore_cache:
            file_metadata, http_response = self.dbx_provider.download(path)
            return serve_from_response(
                http_response,
                name=file_metadata.name
            )
        else:
            file_metadata, fileobj = self.dbx_provider.get_read_fileobj(path)
            return serve_fileobj(
                fileobj,
                content_length=file_metadata.size,
                name=file_metadata.name,
                chunk_size=chunk_size
            )

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @error_handler
    def fetch_thumbnail(self, path):
        """Request thumbnail from local storage."""
        file_metadata, thumbnailfileobj = self.dbx_provider.get_thumbnail_read_fileobj(path)
        return serve_fileobj(
            thumbnailfileobj,
            content_length=thumbnailfileobj.tell(),
            name='thumbnail_{}'.format(file_metadata.name)
        )

    @cherrypy.expose
    @cherrypy.tools.large_upload()
    @cherrypy.tools.allow(methods=('POST',))
    @error_handler
    def push(self, path, ignore_cache='false'):
        """Push a file to storage."""
        fileobj = cherrypy.request.body.fp
        ignore_cache = str_to_bool(ignore_cache)
        if ignore_cache:
            self.dbx_provider.upload_direct(fileobj, path)
        else:
            self.dbx_provider.receive_upload(fileobj, path)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @error_handler
    def whitelist(self, path):
        """Whitelist file for sync."""
        self.dbx_provider.update_whitelisted(path, True)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @error_handler
    def blacklist(self, path):
        """Blacklist file from sync."""
        self.dbx_provider.update_whitelisted(path, False)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @error_handler
    def remove(self, path):
        """Remove a file permanently"""
        self.dbx_provider.remove_cached_file(path)
        self.dbx_provider.remove_remote_file(path)

    @cherrypy.expose
    @error_handler
    def upload_form(self):
        """Return static form for file upload"""
        return open('cloudcache/templates/upload_file.html')


class NotificationServiceAPI(object):
    def __init__(self, notification_service):
        self.notification_service = notification_service

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('POST',))
    @error_handler
    def register(self, token):
        """Register new message recipient."""
        self.notification_service.register(token)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('POST',))
    @error_handler
    def unregister(self, token):
        """Unregister an existing message recipient. Raise exception if
        token is not registered."""
        self.notification_service.unregister(token)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @cherrypy.tools.json_out(handler=json_handler)
    @error_handler
    def is_registered(self, token):
        """Return True if token is registered."""
        return {
            'result': self.notification_service.is_registered(token)
        }


def serve_fileobj(fileobj, content_length, name=None,
                  content_type='application/octet-stream',
                  chunk_size=64*1024):
    """Serve content from file-like object."""
    if name:
        cherrypy.response.headers['Content-Disposition'] = \
            'attachment; filename="%s"' % name
    cherrypy.response.headers['Content-Type'] = content_type
    cherrypy.response.headers['Content-Length'] = content_length
    fg = cherrypy.lib.file_generator_limited(
        fileobj, content_length, chunk_size
    )
    with contextlib.closing(fileobj):
        for chunk in fg:
            yield chunk


def serve_from_response(response, name=None, chunk_size=64*1024):
    """Serve content from HTTP response."""
    copy_headers = ('Content-Length', 'Content-Type', 'Content-Disposition')
    cherrypy.response.headers.update(
        {h_name: response.headers.get(h_name) for h_name in copy_headers}
    )

    if name:
        cherrypy.response.headers['Content-Disposition'] = \
            'attachment; filename="%s"' % name

    return response.iter_content(chunk_size)
qX�  #!/usr/bin/env python2.7

"""
Web APIs.
"""


from __future__ import absolute_import

import cherrypy
import cherrypy.lib
import cherrypy.lib.static
import cherrypy.process.plugins
import contextlib
import functools
from cloudcache.util import json_encoder


def large_upload():
    cherrypy.response.timeout = 3600
    cherrypy.request.process_request_body = False

cherrypy.tools.large_upload = cherrypy.Tool('before_request_body', large_upload)


def errrr_handler(f):
    """Translate exceptions to HTTP response with appropriate status code
    and JSON-serialized response body.

    Exceptions can specify HTTP status code via http_status_code attribute, with
    HTTP status 500 (Internal Server Error) as the default.
    """
    @functools.wraps(f)
    def wrapped(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as err:
            status_code = getattr(err, 'http_status_code', 500)
            cherrypy.log('An exception occured', traceback=True)

            response = cherrypy.serving.response
            response.status = status_code
            response.headers['Content-Type'] = 'application/json'

            return json_encoder.encode(
                {
                    'error': {
                        'type': type(err).__name__,
                        'args': err.args
                    }
                }
            )
    return wrapped


def json_handler(*args, **kwargs):
    """CherryPy request handler using custom JSON encoder."""
    value = cherrypy.serving.request._json_inner_handler(*args, **kwargs)
    return json_encoder.iterencode(value)


def str_to_bool(o):
    return str(o).lower() in ('true', 'yes', 'aye', 'yup', 'totally', 'yessir')


class DropboxAPI(object):
    """Web API for dropbox provider."""

    def __init__(self, dbx_provider=None):
        self.dbx_provider = dbx_provider

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @cherrypy.tools.json_out(handler=json_handler)
    @error_handler
    def status(self):
        """Return storage status from storage provider."""
        return self.dbx_provider.status()

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @error_handler
    def fetch(self, path, ignore_cache='false', chunk_size=64*1024):
        """Request a file from storage."""
        cherrypy.serving.response.stream = True

        ignore_cache = str_to_bool(ignore_cache)
        chunk_size = int(chunk_size)

        if ignore_cache:
            file_metadata, http_response = self.dbx_provider.download(path)
            return serve_from_response(
                http_response,
                name=file_metadata.name
            )
        else:
            file_metadata, fileobj = self.dbx_provider.get_read_fileobj(path)
            return serve_fileobj(
                fileobj,
                content_length=file_metadata.size,
                name=file_metadata.name,
                chunk_size=chunk_size
            )

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @error_handler
    def fetch_thumbnail(self, path):
        """Request thumbnail from local storage."""
        file_metadata, thumbnailfileobj = self.dbx_provider.get_thumbnail_read_fileobj(path)
        return serve_fileobj(
            thumbnailfileobj,
            content_length=thumbnailfileobj.tell(),
            name='thumbnail_{}'.format(file_metadata.name)
        )

    @cherrypy.expose
    @cherrypy.tools.large_upload()
    @cherrypy.tools.allow(methods=('POST',))
    @error_handler
    def push(self, path, ignore_cache='false'):
        """Push a file to storage."""
        fileobj = cherrypy.request.body.fp
        ignore_cache = str_to_bool(ignore_cache)
        if ignore_cache:
            self.dbx_provider.upload_direct(fileobj, path)
        else:
            self.dbx_provider.receive_upload(fileobj, path)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @error_handler
    def whitelist(self, path):
        """Whitelist file for sync."""
        self.dbx_provider.update_whitelisted(path, True)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @error_handler
    def blacklist(self, path):
        """Blacklist file from sync."""
        self.dbx_provider.update_whitelisted(path, False)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @error_handler
    def remove(self, path):
        """Remove a file permanently"""
        self.dbx_provider.remove_cached_file(path)
        self.dbx_provider.remove_remote_file(path)

    @cherrypy.expose
    @error_handler
    def upload_form(self):
        """Return static form for file upload"""
        return open('cloudcache/templates/upload_file.html')


class NotificationServiceAPI(object):
    def __init__(self, notification_service):
        self.notification_service = notification_service

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('POST',))
    @error_handler
    def register(self, token):
        """Register new message recipient."""
        self.notification_service.register(token)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('POST',))
    @error_handler
    def unregister(self, token):
        """Unregister an existing message recipient. Raise exception if
        token is not registered."""
        self.notification_service.unregister(token)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @cherrypy.tools.json_out(handler=json_handler)
    @error_handler
    def is_registered(self, token):
        """Return True if token is registered."""
        return {
            'result': self.notification_service.is_registered(token)
        }


def serve_fileobj(fileobj, content_length, name=None,
                  content_type='application/octet-stream',
                  chunk_size=64*1024):
    """Serve content from file-like object."""
    if name:
        cherrypy.response.headers['Content-Disposition'] = \
            'attachment; filename="%s"' % name
    cherrypy.response.headers['Content-Type'] = content_type
    cherrypy.response.headers['Content-Length'] = content_length
    fg = cherrypy.lib.file_generator_limited(
        fileobj, content_length, chunk_size
    )
    with contextlib.closing(fileobj):
        for chunk in fg:
            yield chunk


def serve_from_response(response, name=None, chunk_size=64*1024):
    """Serve content from HTTP response."""
    copy_headers = ('Content-Length', 'Content-Type', 'Content-Disposition')
    cherrypy.response.headers.update(
        {h_name: response.headers.get(h_name) for h_name in copy_headers}
    )

    if name:
        cherrypy.response.headers['Content-Disposition'] = \
            'attachment; filename="%s"' % name

    return response.iter_content(chunk_size)
q	��q
aGAձ7�cӷ��qhX*   Renaming <event_completed> to <event_done>q]qhUtask.pyqX   #!/usr/bin/env python2.7

import abc
import cherrypy
import datetime
import threading
import uuid


class CancelledException(Exception):
    pass


class TaskBase(object):
    """Base class for long-running, cancellable tasks."""

    RESULT_SUCCESS = 'SUCCESS'
    RESULT_FAILURE = 'FAILURE'
    RESULT_CANCELLED = 'CANCELLED'
    RESULT_UNKNOWN = 'UNKNOWN'

    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.success = None
        self.running = False
        self.cancelled = False
        self.error = None

        self.event_started = threading.Event()
        self.event_done = threading.Event()

        self.timestamp_initialized = datetime.datetime.now()
        self.timestamp_started = None
        self.timestamp_completed = None

        self.log('INITIALIZED')

    @property
    def result_str(self):
        """Return a string describing task result."""
        result_names = {
            True: self.RESULT_SUCCESS,
            False: self.RESULT_FAILURE,
            None: self.RESULT_UNKNOWN
        }

        if self.cancelled and self.success is not None:
            return self.RESULT_CANCELLED
        else:
            return result_names[self.success]

    @abc.abstractmethod
    def _start(self):
        pass

    def status(self):
        return {
            'running': self.running,
            'cancelled': self.cancelled,
            'result': self.result_str,
            'timestamp_initialized': self.timestamp_initialized,
            'timestamp_started': self.timestamp_started,
            'timestamp_completed': self.timestamp_completed,
            'error': self.error
        }

    def log(self, msg, traceback=False):
        cherrypy.log(msg, traceback=traceback)

    def cancel(self):
        if self.running is False and self.success is not None:
            return
        self.log('CANCELLING')
        self.cancelled = True
        self.running = False

    def start(self):
        self.log('STARTING')
        self.running = True
        self.timestamp_started = datetime.datetime.now()
        self.event_started.set()
        try:
            if self.cancelled:
                raise CancelledException()
            self._start()
        except Exception as err:
            self.success = False
            self.error = err
        else:
            self.success = True
        finally:
            self.running = False
            self.timestamp_completed = datetime.datetime.now()
            self.log(
                'COMPLETED: %s' % self.result_str,
                traceback=self.error is not None
            )

    def is_done(self):
        return self.success in (True, False)


class ShortTask(object):
    """Simple class representing a short-running task."""
    def __init__(self, target, args, kwargs):
        self.event_started = threading.Event()
        self.event_done = threading.Event()
        self.result = None
        self.id = uuid.uuid1()
        self.target = target
        self.args = args
        self.kwargs = kwargs

    @property
    def args(self):
        return (self.target, self.args, self.kwargs)

    def is_started(self):
        return self.event_started.is_set()

    def is_done(self):
        return self.event_done.is_set()

    def is_successful(self):
        return not isinstance(self.result, Exception)

    def start(self):
        self.event_started.set()
        try:
            self.result = self.target(*self.args, **self.kwargs)
        except Exception as err:
            self.result = err
        finally:
            self.event_done.set()
qX  #!/usr/bin/env python2.7

import abc
import cherrypy
import datetime
import threading
import uuid


class CancelledException(Exception):
    pass


class TaskBase(object):
    """Base class for long-running, cancellable tasks."""

    RESULT_SUCCESS = 'SUCCESS'
    RESULT_FAILURE = 'FAILURE'
    RESULT_CANCELLED = 'CANCELLED'
    RESULT_UNKNOWN = 'UNKNOWN'

    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.success = None
        self.running = False
        self.cancelled = False
        self.error = None

        self.event_started = threading.Event()
        self.event_completed = threading.Event()

        self.timestamp_initialized = datetime.datetime.now()
        self.timestamp_started = None
        self.timestamp_completed = None

        self.log('INITIALIZED')

    @property
    def result_str(self):
        """Return a string describing task result."""
        result_names = {
            True: self.RESULT_SUCCESS,
            False: self.RESULT_FAILURE,
            None: self.RESULT_UNKNOWN
        }

        if self.cancelled and self.success is not None:
            return self.RESULT_CANCELLED
        else:
            return result_names[self.success]

    @abc.abstractmethod
    def _start(self):
        pass

    def status(self):
        return {
            'running': self.running,
            'cancelled': self.cancelled,
            'result': self.result_str,
            'timestamp_initialized': self.timestamp_initialized,
            'timestamp_started': self.timestamp_started,
            'timestamp_completed': self.timestamp_completed,
            'error': self.error
        }

    def log(self, msg, traceback=False):
        cherrypy.log(msg, traceback=traceback)

    def cancel(self):
        if self.running is False and self.success is not None:
            return
        self.log('CANCELLING')
        self.cancelled = True
        self.running = False

    def start(self):
        self.log('STARTING')
        self.running = True
        self.timestamp_started = datetime.datetime.now()
        self.event_started.set()
        try:
            if self.cancelled:
                raise CancelledException()
            self._start()
        except Exception as err:
            self.success = False
            self.error = err
        else:
            self.success = True
        finally:
            self.running = False
            self.timestamp_completed = datetime.datetime.now()
            self.log(
                'COMPLETED: %s' % self.result_str,
                traceback=self.error is not None
            )

    def is_done(self):
        return self.success in (True, False)


class ShortTask(object):
    """Simple class representing a short-running task."""
    def __init__(self, target, args, kwargs):
        self.event_started = threading.Event()
        self.event_done = threading.Event()
        self.result = None
        self.id = uuid.uuid1()
        self.target = target
        self.args = args
        self.kwargs = kwargs

    @property
    def args(self):
        return (self.target, self.args, self.kwargs)

    def is_started(self):
        return self.event_started.is_set()

    def is_done(self):
        return self.event_done.is_set()

    def is_successful(self):
        return not isinstance(self.result, Exception)

    def start(self):
        self.event_started.set()
        try:
            self.result = self.target(*self.args, **self.kwargs)
        except Exception as err:
            self.result = err
        finally:
            self.event_done.set()
q��qaGAձi�DKZ��qe]qe.