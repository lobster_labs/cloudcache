#!/bin/bash -e

USAGE=$(cat << EOF
Generate a dummy file of arbitrary size.

Usage: $0 SIZE_IN_MB NAME
EOF
)

show_usage(){
    echo "$USAGE"
    exit 1
}

test -z $1 && show_usage
test -z $2 && show_usage

SIZE_IN_MB=$1
NAME=$2

head -c $((SIZE_IN_MB*1024*1024)) /dev/urandom > $NAME

