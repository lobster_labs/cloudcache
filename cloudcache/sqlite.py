#!/usr/bin/env python2.7

import sqlite3
import threading


class SqliteDB(object):
    """Helper class to reduce sqlite3 database boilerplate code."""

    def __init__(self, filepath, schema_filepath):
        self.filepath = filepath
        self.lock = threading.Lock()
        self._row_factory = sqlite3.Row
        self.__local = threading.local()

        self.setup_db(schema_filepath)

    @property
    def connection(self):
        if not hasattr(self.__local, 'connection'):
            conn = sqlite3.connect(
                self.filepath,
                detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
            )
            conn.row_factory = self.row_factory
            self.__local.connection = conn

        return self.__local.connection

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_tb):
        self.close()

    def setup_db(self, schema_filepath):
        with self.lock:
            with sqlite3.connect(self.filepath) as conn:
                with open(schema_filepath) as schema_fh:
                    conn.executescript(schema_fh.read())
                conn.commit()
