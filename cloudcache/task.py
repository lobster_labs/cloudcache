#!/usr/bin/env python2.7

import abc
import cherrypy
import datetime
import threading


class CancelledException(Exception):
    pass


class TaskBase(object):
    """Base class for cancellable tasks."""

    RESULT_SUCCESS = 'SUCCESS'
    RESULT_FAILURE = 'FAILURE'
    RESULT_CANCELLED = 'CANCELLED'
    RESULT_UNKNOWN = 'UNKNOWN'

    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.success = None
        self.running = False
        self.cancelled = False
        self.error = None

        self.event_started = threading.Event()
        self.event_completed = threading.Event()

        self.timestamp_initialized = datetime.datetime.now()
        self.timestamp_started = None
        self.timestamp_completed = None

        self.log('INITIALIZED')

    @property
    def result_str(self):
        """Return a string describing task result."""
        result_names = {
            True: self.RESULT_SUCCESS,
            False: self.RESULT_FAILURE,
            None: self.RESULT_UNKNOWN
        }

        if self.cancelled and self.success is not None:
            return self.RESULT_CANCELLED
        else:
            return result_names[self.success]

    @abc.abstractmethod
    def _start(self):
        pass

    def status(self):
        return {
            'running': self.running,
            'cancelled': self.cancelled,
            'result': self.result_str,
            'timestamp_initialized': self.timestamp_initialized,
            'timestamp_started': self.timestamp_started,
            'timestamp_completed': self.timestamp_completed,
            'error': self.error
        }

    def log(self, msg, traceback=False):
        cherrypy.log(msg, traceback=traceback)

    def cancel(self):
        if self.running is False and self.success is not None:
            return
        self.log('CANCELLING')
        self.cancelled = True
        self.running = False

    def start(self):
        self.log('STARTING')
        self.running = True
        self.timestamp_started = datetime.datetime.now()
        self.event_started.set()
        try:
            if self.cancelled:
                raise CancelledException()
            self._start()
        except Exception as err:
            self.success = False
            self.error = err
        else:
            self.success = True
        finally:
            self.running = False
            self.timestamp_completed = datetime.datetime.now()
            self.log(
                'COMPLETED: %s' % self.result_str,
                traceback=self.error is not None
            )

    def is_done(self):
        return self.success in (True, False)
