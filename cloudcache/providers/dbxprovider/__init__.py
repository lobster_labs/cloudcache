#!/usr/bin/env python2.7

"""Provider for Dropbox."""


from __future__ import absolute_import, unicode_literals

import cherrypy
from cherrypy.lib import file_generator
import cherrypy.process.plugins

from . import db, exceptions, tasks, util
from cloudcache.util import makedirs, CacheFile
from cloudcache.service import SimpleService
import contextlib
import datetime
import dropbox
import dropbox.exceptions
import dropbox.files
import errno
import itertools
import os
import shutil
import threading
import time


class DropboxProvider(SimpleService):
    """Provider for Dropbox. Handles communication with Dropbox API and
    local-remote file state synchronization.

    All storage filepaths should be relative to cloud storage root, e.g.
    "/some_dir/some_file.ext".

    :param work_dirpath: Path to directory to use for storage.
    :param api_key: Dropbox API key.
    :param polling_interval: File synchronization polling interval in seconds.
    :param polling_on_error_interval: If error occurs during remote polling,
    wait for this amount of seconds instead of regular interval.
    :param bus: Bus to subsribe to.
    """
    file_status_keys = (
        'in_cloud',
        'in_cache',
        'in_sync',
        'local_size',
        'local_modified',
        'dbx_size',
        'dbx_modified',
        'whitelisted',
        'has_thumbnail'
    )

    base_dirs = {
        'cache': '.cache',
        'cache-thumbnail': '.cache-thumbnail',
    }

    def __init__(self, work_dirpath, api_key, channel_download, channel_upload,
                 channel_notify, remote_polling_interval=10,
                 local_polling_interval=1, bus=cherrypy.engine):
        super(DropboxProvider, self).__init__(log_prefix='[DROPBOX]', bus=bus)

        # Local workspace
        self.work_dirpath = work_dirpath
        self.store_filepath = os.path.join(self.work_dirpath, 'files.shelf')
        self.cache_dirpath = os.path.join(work_dirpath, self.base_dirs['cache'])
        self.thumbnail_cache_dirpath = \
            os.path.join(work_dirpath, self.base_dirs['cache-thumbnail'])
        self.fs_bytes_free = None
        self.fs_bytes_total = None
        self.store = None

        # Client
        self.client = dropbox.Dropbox(api_key)

        # Current status
        self.file_status = {}
        self.running = False

        # Polling
        self.local_polling_interval = local_polling_interval
        self.remote_polling_interval = remote_polling_interval

        self.local_polling_thread = None
        self.remote_polling_thread = None

        # Remote listing; updated by polling thread
        self.dbx_listing = []
        self.dbx_listing_error = None
        self.dbx_listing_timestamp = None

        # Uploads/downloads
        self.downloads = []
        self.uploads = []

        self.channel_download = channel_download
        self.channel_upload = channel_upload
        self.channel_notify = channel_notify

        self.log('INITIALIZED')

    def _setup_workspace(self):
        """Create work directory structure, get FS stats."""
        for path in (
            self.work_dirpath,
            self.cache_dirpath,
            self.thumbnail_cache_dirpath
        ):
            makedirs(path, exist_ok=True)

        self.fs_bytes_free, self.fs_bytes_total = self.get_local_fs_stats()

    def _setup_store(self):
        """Return file metastore."""
        self.store = db.FileMetaStore(self.store_filepath)

    def _init_polling_threads(self):
        for t in self.polling_threads:
            if t is not None and t.is_alive():
                raise threading.ThreadError('%s: thread still alive' % t)

        self.remote_polling_thread = threading.Thread(target=self._poll_remote)
        self.local_polling_thread = threading.Thread(target=self._poll_local)

        for t in self.polling_threads:
            t.daemon = True
            t.start()

    @property
    def polling_threads(self):
        return (self.remote_polling_thread, self.local_polling_thread)

    def start(self):
        self.log('STARTING')
        self._setup_workspace()
        self._setup_store()
        self.running = True
        self._init_polling_threads()
        self.log('STARTED')

    def stop(self):
        self.log('STOPPING')
        self.running = False

        for thread in self.polling_threads:
            if thread is not None and thread.is_alive():
                thread.join()

        for task in itertools.chain(self.downloads, self.uploads):
            try:
                task.cancel()
            except exceptions.CancelledException:
                pass
            except Exception:
                self.log(
                    'Error when cancelling task: %s' % task,
                    traceback=True
                )

        self.log('STOPPED')

    def status(self):
        status = {}
        status.update(self.file_status)
        status.update({
            'downloads': [d.status() for d in self.downloads],
            'uploads': [u.status() for u in self.uploads]
        })
        return status

    def update_whitelisted(self, path, whitelisted):
        """Update 'whitelisted' attribute of cached file.

        :param path: Filepath to stored file
        :param whitelisted: New value for 'whitelisted' attribute
        :type whitelisted: bool

        :raises DropboxFileNotFoundException
        """
        meta = self.store.get_by_path(path)
        if meta:
            if meta['is_whitelisted'] is not whitelisted:
                meta['is_whitelisted'] = whitelisted
                self.store.upsert(**meta)
        else:
            raise exceptions.DropboxFileNotFoundException(path)

    def _poll_remote(self):
        interval = self.remote_polling_interval
        while self.running:
            try:
                self.dbx_listing = list(self.fetch_dbx_listing())
                self.dbx_listing_timestamp = datetime.datetime.now()
                self.dbx_listing_error = None
                interval = self.remote_polling_interval
            except Exception as err:
                self.log(
                    'Error while retrieving remote listing',
                    traceback=True
                )
                self.dbx_listing_error = err
                interval += int(0.5 * interval)
            finally:
                time.sleep(interval)

    def _poll_local(self):
        while self.running:
            try:
                dbx_listing = self.dbx_listing[:]
                dbx_listing_error = self.dbx_listing_error
                dbx_listing_timestamp = self.dbx_listing_timestamp

                if not dbx_listing_error:
                    self.do_delta(
                        local_listing=self.get_local_listing(),
                        dbx_listing=dbx_listing,
                        dbx_listing_timestamp=dbx_listing_timestamp
                    )

                self.do_local_cleanup(
                    local_listing=self.get_local_listing(),
                    dbx_listing=dbx_listing,
                    dbx_listing_error=dbx_listing_error,
                    dbx_listing_timestamp=dbx_listing_timestamp
                )

                self.finalize_tasks(self.uploads)
                self.finalize_tasks(self.downloads)

                self.update_local_status(
                    local_listing=self.get_local_listing(),
                    dbx_listing=dbx_listing,
                    dbx_listing_timestamp=dbx_listing_timestamp,
                    dbx_listing_error=self.dbx_listing_error
                )
            except Exception:
                self.log('Error while updating local status', traceback=True)
            finally:
                time.sleep(self.local_polling_interval)

    def do_delta(self, local_listing, dbx_listing, dbx_listing_timestamp):
        """Compare local and remote state. Disposition actions to keep two
        repositories in sync."""
        remote_by_path_lower = util.group_by_path_lower(dbx_listing)
        local_by_path_lower = util.group_by_path_lower(local_listing)

        for path, dbx_file_metadata in remote_by_path_lower.iteritems():
            is_dir = isinstance(dbx_file_metadata, dropbox.files.FolderMetadata)

            local_entity_metadata = local_by_path_lower.get(path)

            if local_entity_metadata is None:  # New file/folder
                self.store.upsert(
                    path=path,
                    is_dir=is_dir
                )
                continue

            if is_dir:  # That's all we do for directories!
                continue

            # Disposition upload/download if local and remote files do not
            # match.
            if not local_entity_metadata == dbx_file_metadata:

                if local_entity_metadata.to_be_uploaded:
                    self.enqueue_upload(path)
                elif (
                    local_entity_metadata.whitelisted and
                    local_entity_metadata.last_synced < dbx_listing_timestamp
                ):
                    self.enqueue_download(
                        path,
                        dbx_file_metadata.id,
                        dbx_file_metadata.rev
                    )

        # Disposition upload of files not found on remote.
        for local_entity_metadata in local_by_path_lower.itervalues():
            if local_entity_metadata.path_lower in remote_by_path_lower:
                continue

            if local_entity_metadata.to_be_uploaded:
                self.enqueue_upload(local_entity_metadata.path)

    def do_local_cleanup(self, local_listing, dbx_listing, dbx_listing_error,
                         dbx_listing_timestamp):
        """Remove stale cache files and metadata."""
        remote_by_path_lower = util.group_by_path_lower(dbx_listing)
        for local_entity_metadata in local_listing:
            if local_entity_metadata.to_be_uploaded:
                if local_entity_metadata.cache_exists():
                    continue
                else:
                    self.log(
                        'CLEANUP: For-upload file missing cache file: %s'
                        % local_entity_metadata.path_lower
                    )
                    self.store.upsert(
                        path=local_entity_metadata.path,
                        to_be_uploaded=False
                    )

            if (
                not local_entity_metadata.whitelisted and
                local_entity_metadata.cache_exists()
            ):
                self.log(
                    'CLEANUP: File blacklisted. Removing from cache: %s '
                    % local_entity_metadata.path_lower
                )
                local_entity_metadata.unlink_cache_files()

            elif (
                dbx_listing_error is None and
                dbx_listing_timestamp is not None
            ):
                if (
                    local_entity_metadata.path_lower not in remote_by_path_lower and
                    local_entity_metadata.last_synced < dbx_listing_timestamp
                ):
                    self.log(
                        'CLEANUP: File missing on remote. Removing: %s '
                        % local_entity_metadata.path_lower
                    )
                    local_entity_metadata.unlink_cache_files()
                    self.store.remove(local_entity_metadata.path)

    def finalize_tasks(self, tasks):
        """Call finalize() on all tasks from the provided list. Modify provided
        list in-place so that it contains only active tasks."""
        active_tasks = []
        for task in tasks:
            if task.is_done():
                try:
                    task.finalize()
                except Exception:
                    self.log('Error finalizing task %s' % task, traceback=True)
                    try:
                        task.cleanup()
                    except Exception:
                        self.log(
                            'Error on task cleanup: %s' % task, traceback=True
                        )
            else:
                active_tasks.append(task)

        tasks[:] = active_tasks[:]

    def update_local_status(self, local_listing, dbx_listing,
                            dbx_listing_timestamp, dbx_listing_error):
        """Update local file status. Last retrieved remote listing is used
        to compare file metadata and decide if files are in sync."""
        dbx_files_by_path = util.group_by_path_lower(dbx_listing)

        files = {}
        for local_entity_metadata in local_listing:
            path_lower = local_entity_metadata.path_lower

            status = dict.fromkeys(self.file_status_keys, None)
            dbx_entity_metadata = dbx_files_by_path.get(path_lower)

            status['is_dir'] = local_entity_metadata.is_dir
            status['has_thumbnail'] = local_entity_metadata.thumbnail_exists()
            in_cache = local_entity_metadata.cache_exists()
            if in_cache:
                status['in_cache'] = in_cache
                status['local_size'] = local_entity_metadata.size
                status['local_modified'] = local_entity_metadata.modified

            if dbx_entity_metadata is not None:
                dbx_is_dir = isinstance(
                    dbx_entity_metadata, dropbox.files.FolderMetadata
                )
                status['in_cloud'] = True
                status['dbx_size'] = \
                    dbx_entity_metadata.size if not dbx_is_dir else None
                status['dbx_modified'] = \
                    dbx_entity_metadata.server_modified if not dbx_is_dir else None
            else:
                if not self.dbx_listing_error:
                    status['in_cloud'] = False
            status['whitelisted'] = local_entity_metadata.whitelisted
            status['in_sync'] = local_entity_metadata == dbx_entity_metadata
            files[path_lower] = status

        self.fs_bytes_free, self.fs_bytes_total = self.get_local_fs_stats()
        self.file_status = {
            'dbx_listing_timestamp': self.dbx_listing_timestamp,
            'dbx_listing_error': self.dbx_listing_error,
            'files': files,
            'fs_bytes_free': self.fs_bytes_free,
            'fs_bytes_total': self.fs_bytes_total
        }


    def download(self, path):
        """Request a download from Dropbox API.

        :param path: Filepath to download. Should start with '/'.
        :rtype (dropbox.files.FileMetadata, requests.models.Response)

        :raises DropboxProviderException, DropboxFileNotFoundException
        """
        try:
            return self.client.files_download(path)
        except dropbox.exceptions.ApiError as err:
            err = err.error
            if isinstance(err.get_path(), dropbox.files.LookupError):
                raise exceptions.DropboxFileNotFoundException(path)
            else:
                raise exceptions.DropboxProviderException(err.err)

    def download_thumbnail(self, path):
        """Request a thumbnail from Dropbox API.

        :param path: Filepath to thumbnail's download. Should start with '/'.
        :rtype (dropbox.files.FileMetadata, requests.models.Response)

        :raises DropboxProviderException, DropboxThumbnailNotExists
        """
        try:
            return self.client.files_get_thumbnail(path)
        except dropbox.exceptions.ApiError as err:
            err = err.message
            if isinstance(err, dropbox.files.ThumbnailError):
                raise exceptions.DropboxThumbnailNotExists()
            else:
                raise exceptions.DropboxProviderException()

    def enqueue_download(self, path, id_=None, rev=None):
        """Disposition a download to Download Service. If download is already
        queued or active, do nothing.

        :param path: Filepath to download. Should start with '/'.
        :param id_: Dropbox ID of file. Used to find duplicate downloads.
        :param rev: Dropbox revision of file. Used to find duplicate downloads.

        """
        allow_thumbnail_download = True

        for u in self.uploads:
            if u.path == path:
                return

        for d in self.downloads:
            if (
                isinstance(d, tasks.DropboxDownloadTask) and
                d.path == path
            ):
                return
            if (
                isinstance(d, tasks.DropboxThumbnailDownloadTask) and
                d.path == path
            ):
                allow_thumbnail_download = False

        self.log('Queuing download: %s %s %s' % (path, id_, rev))

        if allow_thumbnail_download:
            thumbnail_download = tasks.DropboxThumbnailDownloadTask(self, path)
            thumbnail_download.start()
            self.downloads.append(thumbnail_download)

        download = tasks.DropboxDownloadTask(
            self, path, channel_notify=self.channel_notify
        )
        self.bus.publish(self.channel_download, download.start)
        self.downloads.append(download)

    def upload_direct(self, fileobj, path, chunk_size=1*1024**2):
        """Upload a file to Dropbox.
        :param fileobj: A file-like object to read upload data from.
        :param path: Destination filepath. Should start with '/'.
        :param chunk_size: Upload chunk size. Should be smaller than 150MB.
        """
        self.log('UPLOAD: Start pass-through: %s' % path)
        upload_wrapper = self.get_upload_wrapper(path)
        with contextlib.closing(fileobj):
            fg = file_generator(fileobj, chunk_size)
            for chunk in fg:
                upload_wrapper.send(chunk)
        file_metadata = upload_wrapper.finalize()
        self.log('UPLOAD: Finish pass-through:  %s' % str(file_metadata))

    def receive_upload(self, fileobj, path, chunk_size=64*1024):
        """Receive upload from client."""
        self.log('UPLOAD: Start: %s' % path)
        with CacheFile(self.cache_dirpath) as cache_f:
            shutil.copyfileobj(fileobj, cache_f, chunk_size)
        cache_filepath = os.path.relpath(cache_f.name, self.cache_dirpath)
        self.store.upsert(
            path=path,
            cache_filepath=cache_filepath,
            dbx_id=None,
            dbx_rev=None,
            is_to_be_uploaded=True,
            last_synced=datetime.datetime.now()
        )
        self.log('UPLOAD: Finished: %s' % path)

    def enqueue_upload(self, path):
        """Disposition an upload to Upload Service."""
        for u in self.uploads:
            if u.path == path:
                return

        for d in self.downloads:
            if d.path == path:
                d.cancel()

        self.log('Queuing upload: %s' % path)

        upload_task = tasks.DropboxUploadTask(self, path)
        self.uploads.append(upload_task)
        self.bus.publish(self.channel_upload, upload_task.start)

    def get_upload_wrapper(self, path):
        """Return a wrapper object for uploading a file to Dropbox API.

        :param path: Destination filepath. Should start with '/'.
        :rtype DropboxUploadWrapper
        """
        upload_wrapper = util.DropboxUploadWrapper(self, path)
        return upload_wrapper

    def fetch_dbx_listing(self, path='/', cursor=None, recursive=True):
        """Fetch remote file listing from Dropbox API.

        :param path: Dirpath to list files in. Should start with '/'.
        :param cursor: Cursor for previous directory state.
        :param recursive: Set to True to list files recursively.
        """
        if path == '/':
            path = path.lstrip('/')

        if not cursor:
            result = self.client.files_list_folder(path, recursive)
        else:
            result = self.client.files_list_folder_continue(cursor)

        while True:
            for entry in result.entries:
                yield entry
            if not result.has_more:
                break
            result = self.client.files_list_folder_continue(result.cursor)

    def get_local_listing(self, path='/'):
        """Return local file listing.

        :param path: Dirpath to list files in. Should be relative to cache root
                     directory and start with '/'.
        """
        for meta in self.store.search(
            condition=lambda x: x['path'].startswith(path)
        ):
            yield LocalEntityMetadata(self, **meta)

    def get_local_entity_metadata(self, path):
        meta = self.store.get_by_path(path)
        return LocalEntityMetadata(self, **meta)

    def get_local_entity_metadata_if_exists(self, store, path):
        meta = self.store.get_by_path(path)
        return None if meta is None else LocalEntityMetadata(self, **meta)

    def get_read_fileobj(self, path):
        """Return a read-only file-like object for a cache file.

        :param path: Filepath to return file handler for. Should start with '/'.
        :rtype (FileMetadata, file)
        :raises DropboxFileNotFoundException
        """
        for download in self.downloads:
            if download.path == path and download.started:
                fileobj = download.get_read_fileobj()
                file_metadata = download.dbx_file_metadata
                break
        else:
            file_metadata = self.get_local_entity_metadata(path)
            try:
                fileobj = file_metadata.get_read_fileobj()
            except IOError as err:
                if err.errno != errno.ENOENT:
                    raise
                raise exceptions.DropboxFileNotFoundException(path)
        return file_metadata, fileobj

    def get_thumbnail_read_fileobj(self, path):
        """Return a read-only file-like object for a thumbnail file.

        :param path: Filepath to return file handler for its thumbnail. Should
                     start with '/'.
        :rtype (FileMetadata, file)
        :raises DropboxThumbnailNotExists
        """
        try:
            file_metadata = self.get_local_entity_metadata(path)
            return file_metadata, file_metadata.get_thumbnail_read_fileobj()
        except Exception:
            raise exceptions.DropboxThumbnailNotExists()

    def get_local_fs_stats(self):
        """Return amount of free and total space on cache dir in bytes"""
        stat = os.statvfs(self.cache_dirpath)
        free = stat.f_bavail * stat.f_frsize
        total = stat.f_blocks * stat.f_frsize
        return free, total

    def remove_cached_file(self, path):
        """Remove cached file with its thumbnail and metadata

        :param path: Filepath to be removed. Should start with '/'.
        """
        file_metadata = self.get_local_entity_metadata_if_exists(path)
        if file_metadata is not None:
            file_metadata.unlink_cache_files()
            self.store.remove(file_metadata.path)
            self.log('REMOVE: cached entity removed: %s' % path)

    def remove_remote_file(self, path):
        """Remove dropbox file

        :param path: Filepath to be removed. Should start with '/'.
        """
        try:
            self.client.files_delete(path)
        except dropbox.exceptions.ApiError:
            raise exceptions.DropboxFileNotFoundException()

        self.log('REMOVE: file removed: %s' % path)


class LocalEntityMetadata(object):
    """Local cached file.

    *_filepath attributes should be relative to provider's cache directory.

    :param provider: DropboxProvider instance.
    :param path: Filepath to cached file. Should be relative to cache root
                 directory and start with '/'.
    """
    def __init__(self, provider, **kwargs):
        self.provider = provider

        # Base file metadata
        self.path = kwargs.pop('path')
        self.cache_filepath = kwargs.pop('cache_filepath', None)
        self.thumbnail_filepath = kwargs.pop('thumb_filepath', None)
        self.id = kwargs.pop('dbx_id', None)
        self.rev = kwargs.pop('dbx_rev', None)
        self.is_dir = kwargs.pop('is_dir', False)
        self.whitelisted = kwargs.pop('is_whitelisted', False)
        self.to_be_uploaded = kwargs.pop('is_to_be_uploaded', False)
        self.last_synced = kwargs.pop('last_synced') or datetime.datetime(1970, 1, 1)

    def __eq__(self, other):
        if other is None:
            return False

        return (
            self.id == other.id and
            self.rev == other.rev and
            self.size == other.size
        )

    def cache_exists(self):
        realpath = self.realpath
        if realpath:
            return os.path.exists(realpath)
        else:
            return False

    def thumbnail_exists(self):
        realpath = self.thumbnail_realpath
        return False if realpath is None else os.path.exists(realpath)

    @property
    def realpath(self):
        if self.cache_filepath:
            return os.path.join(
                self.provider.cache_dirpath, self.cache_filepath
            )

    @property
    def thumbnail_realpath(self):
        if self.thumbnail_filepath is not None:
            return os.path.join(
                self.provider.thumbnail_cache_dirpath, self.thumbnail_filepath
            )

    def get_read_fileobj(self):
        if self.realpath is None:
            raise OSError(errno.ENOENT)
        return open(self.realpath, 'rb')

    def get_thumbnail_read_fileobj(self):
        return open(self.thumbnail_realpath, 'rb')

    def unlink_cache_files(self):
        for path in (self.realpath, self.thumbnail_realpath):
            if not path:
                continue
            try:
                os.unlink(path)
            except OSError as err:
                if err.errno != errno.ENOENT:
                    raise

    @property
    def name(self):
        return os.path.basename(self.path)

    @property
    def path_lower(self):
        return self.path.lower()

    @property
    def modified(self):
        """Return cache file's last modified timestamp."""
        if self.cache_exists():
            stat = os.stat(self.realpath)
            return datetime.datetime.fromtimestamp(stat.st_mtime)
        return None

    @property
    def size(self):
        if self.cache_exists():
            stat = os.stat(self.realpath)
            return stat.st_size
        return None
