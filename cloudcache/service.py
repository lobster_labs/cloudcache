#!/usr/bin/env python2.7

from __future__ import absolute_import

import cherrypy
from cherrypy.process.plugins import SimplePlugin
from cloudcache.util import FromConfigMixin, BusSubscribeMixin
import Queue
from multiprocessing.dummy import Pool
import threading


class SimpleService(
    BusSubscribeMixin,
    FromConfigMixin,
    SimplePlugin
):
    """Base class for services integrated with CherryPy's bus."""

    _cp_skip_channels = ('log',)

    def __init__(self, log_prefix, bus=cherrypy.engine):
        super(SimpleService, self).__init__(bus=bus)
        self.log_prefix = log_prefix

    def log(self, msg, traceback=False):
        cherrypy.log('%s %s' % (self.log_prefix, msg), traceback=traceback)


class WorkerPoolService(SimpleService):
    """Base class for listen-queue-run services integrated with CherryPy's
    bus."""

    def __init__(self, channel_enqueue, n_workers=1, log_prefix='',
                 bus=cherrypy.engine):
        super(WorkerPoolService, self).__init__(log_prefix=log_prefix, bus=bus)

        self.channel_enqueue = channel_enqueue
        self.queue = Queue.Queue()

        self.running = False
        self.worker_pool = Pool(n_workers)
        self.thread_process_queue = None

        self.log('INITIALIZED')

    def _init_queue_processing_thread(self):
        if self.thread_process_queue and self.thread_process_queue.is_alive():
            raise threading.ThreadError('Still alive')

        self.thread_process_queue = threading.Thread(target=self._process_queue)
        self.thread_process_queue.daemon = True
        self.thread_process_queue.start()

    def _process_queue(self):
        while self.running:
            try:
                func, callback = self.queue.get(timeout=5)
                if not getattr(func, '__call__'):
                    raise ValueError('%s is not callable' % func)
            except Queue.Empty:
                pass
            else:
                self.worker_pool.apply_async(func, callback=callback)
                self.queue.task_done()

    def start(self):
        self.log('STARTING')
        self.on_before_start()
        self.running = True
        self._init_queue_processing_thread()
        self.bus.subscribe(self.channel_enqueue, self.enqueue)
        self.on_after_start()
        self.log('STARTED')

    def stop(self):
        self.log('STOPPING')
        self.on_before_stop()
        self.bus.unsubscribe(self.channel_enqueue, self.enqueue)
        self.running = False
        self.worker_pool.close()
        self.worker_pool.join()
        self.thread_process_queue.join()
        self.on_after_stop()
        self.log('STOPPED')

    def enqueue(self, *args):
        item = args[0]
        callback = args[1] if len(args) > 1 else None
        self.queue.put((item, callback))

    def on_before_start(self):
        pass

    def on_after_start(self):
        pass

    def on_before_stop(self):
        pass

    def on_after_stop(self):
        pass