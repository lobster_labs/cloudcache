#!/usr/bin/env python2.7

from __future__ import absolute_import

from ... import sqlite

import pkg_resources
import sqlite3


class FileMetaStore(sqlite.SqliteDB):
    """A basic data store for local entity metadata."""
    def __init__(self, filepath):
        super(FileMetaStore, self).__init__(
            filepath,
            pkg_resources.resource_filename(__name__, 'db_schema.sql')
        )
        self.row_factory = row_factory

    def get_by_path(self, path=None):
        q_select_filemeta = \
            '''
            SELECT * FROM file
            WHERE path=:path
            ;
            '''

        cursor = self.connection.cursor()
        with self.lock:
            cursor.execute(q_select_filemeta, {'path': path})
            return cursor.fetchone()

    def get_all(self):
        q_select_all = \
            '''
            SELECT * from file
            ;
            '''
        cursor = self.connection.cursor()
        with self.lock:
            cursor.execute(q_select_all)

        for item in cursor:
            yield item

    def search(self, condition):
        for item in self.get_all():
            if condition(item):
                yield item

    def upsert(self, path, **kwargs):
        path = path.lower()
        meta = self.get_by_path(path) or {'path': path}
        meta.update(kwargs)

        q_insert_filemeta = \
            '''
            INSERT OR REPLACE INTO file ({o_colnames})
            VALUES ({o_fieldnames})
            ;
            '''
        q_insert_filemeta = q_insert_filemeta.format(
            o_colnames=','.join(k for k in meta),
            o_fieldnames=','.join(':' + k for k in meta)
        )

        cursor = self.connection.cursor()
        with self.lock:
            cursor.execute(q_insert_filemeta, meta)
            self.connection.commit()

    def remove(self, path):
        q_remove_filemeta = \
            '''
            DELETE FROM file
            WHERE path=:path
            '''
        cursor = self.connection.cursor()
        with self.lock:
            cursor.execute(q_remove_filemeta, {'path': path})
            self.connection.commit()


def row_factory(cursor, row):
    data = dict(sqlite3.Row(cursor, row))

    cast_keys = {
        'is_dir': bool,
        'is_whitelisted': bool,
        'is_to_be_uploaded': bool,
    }

    for k, f in cast_keys.iteritems():
        data[k] = f(data[k])

    return data
