#!/usr/bin/env python2.7

"""
Notification-related functionality.
"""

from __future__ import absolute_import

import cherrypy
from cloudcache.util import (
    makedirs,
    pretty_printer,
    JSONEncoder
)
from cloudcache.service import WorkerPoolService
from functools import partial
from gcm import gcm
import json
import os
import shelve
import threading

CHANNEL_NOTIFY = 'notify'


class NotificationServiceException(Exception):
    pass


class RecipientNotFoundError(NotificationServiceException, KeyError):
    http_status_code = 404


# Monkey-patch gcm.JsonPayload to accept custom JSON encoder
class JsonPayload(gcm.Payload):
    @property
    def body(self):
        kwargs = dict(self.__dict__)
        json_encoder = kwargs.pop('json_encoder', None)
        return json.dumps(kwargs, cls=json_encoder)

gcm.JsonPayload = JsonPayload


class NotificationService(WorkerPoolService):
    """Send Google Cloud Message notifications to predefined recipient list.

    Uses shelve module for simple persistence.

    :param gcm_client: Google Cloud Messaging client.
    :type gcm_client: gcm.GCM
    :param api_key: Google Cloud Messaging API key.
    :param store_filepath: Path to file to be used by for persistent registation
                           ID store.
    :param bus: Bus to subscribe to.
    """

    def __init__(self, api_key, store_filepath, bus=cherrypy.engine):
        self.gcm_client = gcm.GCM(api_key)
        self.store_write_lock = threading.Lock()
        self.store_filepath = store_filepath
        self.store = None

        super(NotificationService, self).__init__(
            channel_enqueue=CHANNEL_NOTIFY,
            n_workers=1,
            log_prefix='[NOTIFICATION-SERVICE]',
            bus=bus
        )

    def on_before_start(self):
        self.setup_store()

    def on_after_stop(self):
        self.store.close()

    def enqueue(self, *args):
        """Put notification data in queue.

        :param data: JSON-serializable data to send as notification payload.
        :param callback: A function to be called when task is finished.
        """
        data = args[0]
        callback = args[1] if len(args) > 1 else None
        func = partial(self.send_notification, data)
        self.queue.put((func, callback))

    def setup_store(self):
        makedirs(os.path.dirname(self.store_filepath), exist_ok=True)
        self.store = shelve.open(self.store_filepath, writeback=True)
        self.store.setdefault('registration_ids', set())

    def send_notification(self, data):
        """Send notification with 'data' payload to a stored list of
        registration ID's.

        :param data: JSON-serializable data to send as notification payload.

        """
        if not self.store['registration_ids']:
            self.log('No recipees registered. Skipping notification.')
            return
        else:
            self.log(
                'Sending notification to %s recipient(s):\n%s'
                % (len(self.store['registration_ids']), pretty_printer.pformat(data))
            )
        notification = {
            'registration_ids': list(self.store['registration_ids']),
            'data': data,
            'priority': 'normal',
            'collapse_key': 'cloudcache_notification',
            'delay_while_idle': False,
            'json_encoder': JSONEncoder
        }
        response = self.gcm_client.json_request(**notification)
        self.log('Response:\n%s' % pretty_printer.pformat(response))

        # Remove invalid registration_ids
        for error, items in response.get('errors', {}).iteritems():
            if error in (
                'InvalidRegistration',
                'InvalidPackageName',
                'NotRegistered'
            ):
                for item in items:
                    self.unregister(item)

    def is_registered(self, registration_id):
        """Return True if given GCM registration ID is registered.

        :param registration_id: A Google Cloud Messaging registration ID.
        """
        return registration_id in self.store['registration_ids']

    def register(self, registration_id):
        """Register a new GCM registration ID. Ignore if registration_id is
        already registered.

        :param registration_id: A Google Cloud Messaging registration ID.
        """
        with self.store_write_lock:
            self.store['registration_ids'].add(registration_id)
            self.store.sync()
        self.log('Registered: \'%s\'' % registration_id)

    def unregister(self, registration_id):
        """Unregister a GCM registration ID. Raise exception if registration ID
        is not registered.

        :param registration_id: A Google Cloud Messaging registration ID.
        :raises RecipientNotFoundError
        """
        with self.store_write_lock:
            try:
                self.store['registration_ids'].remove(registration_id)
                self.store.sync()
                self.log('Unregistered: \'%s\'' % registration_id)
            except KeyError:
                raise RecipientNotFoundError(registration_id)
