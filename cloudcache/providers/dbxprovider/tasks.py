#!/usr/bin/env python2.7

from __future__ import absolute_import


from cherrypy.lib import file_generator
from cloudcache.util import CacheFile, LiveFileReader
from cloudcache.updown import DownloadTaskBase, UploadTaskBase
import datetime
from .import exceptions
import contextlib
import os
import threading


class DropboxDownloadTask(DownloadTaskBase):
    """Dropbox provider download.

    :param provider: DropboxProvider instance.
    :param path: Destination path relative to root dirpath. Should start with
                 '/'.
    :param chunk_size: Chunk size to use during read from HTTP response.
    """

    def __init__(self, provider, path, channel_notify=None, chunk_size=64*1024):
        self.provider = provider
        self.dbx_file_metadata = None
        self.chunk_size = chunk_size
        self.channel_notify = channel_notify
        self._write_fh = None
        self.event_file_available = threading.Event()
        super(DropboxDownloadTask, self).__init__(path)

    @property
    def id(self):
        if self.dbx_file_metadata:
            return self.dbx_file_metadata.id

    @property
    def rev(self):
        if self.dbx_file_metadata:
            return self.dbx_file_metadata.rev

    def _start(self):
        """Execute download from Dropbox."""
        dbx_file_metadata, http_response = self.provider.download(self.path)
        self.dbx_file_metadata = dbx_file_metadata
        self.bytes_total = dbx_file_metadata.size

        http_r = http_response.iter_content(self.chunk_size)
        with contextlib.closing(http_r):
            with CacheFile(self.provider.cache_dirpath) as write_fh:
                self._write_fh = write_fh
                self.event_file_available.set()
                for chunk in http_r:

                    if not self.running:
                        raise exceptions.CancelledException

                    write_fh.write(chunk)
                    self.bytes_downloaded += len(chunk)

    def finalize(self):
        if not self.success:
            return
        self._update_store()
        if self.channel_notify is not None:
            self.provider.bus.publish(self.channel_notify, self.status())
        self.log('FINALIZED')

    def cleanup(self):
        if self._write_fh:
            os.unlink(self._write_fh.name)

    def _update_store(self):
        cache_filepath = os.path.relpath(
            self._write_fh.name, self.provider.cache_dirpath
        )
        self.provider.store.upsert(
            path=self.path,
            cache_filepath=cache_filepath,
            dbx_id=self.id,
            dbx_rev=self.rev,
            last_synced=datetime.datetime.now()
        )

    def get_read_fileobj(self, timeout=5):
        """Return a file-like object for reading from in-progress download.

        :param timeout: Max time (in seconds) to wait for file availability.
        """
        self.event_started.wait()
        self.event_file_available.wait(timeout)
        return LiveFileReader(
            self._write_fh,
            expected_size=self.bytes_total
        )

    def log(self, msg, traceback=False):
        prefix = \
            '[DROPBOX-DOWNLOAD] {path} (id:{id} rev:{rev}) {bytes_d}/{bytes_t}'\
            .format(path=self.path, id=self.id, rev=self.rev,
                    bytes_d=self.bytes_downloaded,
                    bytes_t=self.bytes_total or '??')
        msg = ' '.join((prefix, msg))
        super(DropboxDownloadTask, self).log(msg, traceback)


class DropboxThumbnailDownloadTask(DownloadTaskBase):
    """Dropbox provider thumbnail download.

    :param provider: DropboxProvider instance.
    :param path: Destination path relative to root dirpath. Should start with
                 '/'.
    :param chunk_size: Chunk size to use during read from HTTP response.
    """

    def __init__(self, provider, path, chunk_size=64*1024):
        self.provider = provider
        self.dbx_file_metadata = None
        self.chunk_size = chunk_size
        self._write_fh = None
        super(DropboxThumbnailDownloadTask, self).__init__(path)

    @property
    def id(self):
        if self.dbx_file_metadata:
            return self.dbx_file_metadata.id

    @property
    def rev(self):
        if self.dbx_file_metadata:
            return self.dbx_file_metadata.rev

    def _start(self):
        """Execute thumbnail's download from Dropbox."""
        dbx_file_metadata, http_response = \
            self.provider.download_thumbnail(self.path)

        self.dbx_file_metadata = dbx_file_metadata
        self.bytes_total = dbx_file_metadata.size

        http_r = http_response.iter_content(self.chunk_size)
        with contextlib.closing(http_r):
            with CacheFile(self.provider.thumbnail_cache_dirpath) as write_fh:
                self._write_fh = write_fh
                for chunk in http_r:

                    if not self.running:
                        raise exceptions.CancelledException

                    write_fh.write(chunk)
                    self.bytes_downloaded += len(chunk)

    def finalize(self):
        if not self.success:
            return
        self._update_store()
        self.log('FINALIZED')

    def cleanup(self):
        if self._write_fh:
            os.unlink(self._write_fh.name)

    def _update_store(self):
        thumbnail_filepath = os.path.relpath(
            self._write_fh.name, self.provider.thumbnail_cache_dirpath
        )
        self.provider.store.upsert(
            path=self.path,
            thumb_filepath=thumbnail_filepath
        )

    def get_read_fileobj(self, timeout):
        pass

    def log(self, msg, traceback=False):
        prefix = \
            '[DROPBOX-THUMBNAIL-DOWNLOAD] {path} (id:{id} rev:{rev}) '\
            '{bytes_d}/{bytes_t}'\
            .format(path=self.path, id=self.id, rev=self.rev,
                    bytes_d=self.bytes_downloaded,
                    bytes_t=self.bytes_total or '??')
        msg = ' '.join((prefix, msg))
        super(DropboxThumbnailDownloadTask, self).log(msg, traceback)


class DropboxUploadTask(UploadTaskBase):
    """Dropbox provider upload.

    :param provider: DropboxProvider instance.
    :param path: Destination path relative to root dirpath. Should start with
                 '/'.
    :param chunk_size: Chunk size to upload during a single HTTP request
    """
    def __init__(self, provider, path, channel_notify=None,
                 chunk_size=1*1024**2):
        self.provider = provider
        self.channel_notify = channel_notify
        self.chunk_size = chunk_size
        self.dbx_file_metadata = None
        self.event_file_available = threading.Event()
        self.upload_wrapper = None
        super(DropboxUploadTask, self).__init__(path)

    @property
    def id(self):
        if self.dbx_file_metadata:
            return self.dbx_file_metadata.id

    @property
    def rev(self):
        if self.dbx_file_metadata:
            return self.dbx_file_metadata.rev

    def _start(self):
        local_file_metadata = self.provider.get_local_entity_metadata(self.path)
        self.bytes_total = local_file_metadata.size
        self.upload_wrapper = self.provider.get_upload_wrapper(self.path)
        read_fileobj = local_file_metadata.get_read_fileobj()
        with contextlib.closing(read_fileobj):
            for chunk in file_generator(read_fileobj, self.chunk_size):
                if self.cancelled:
                    raise exceptions.CancelledException()
                self.upload_wrapper.send(chunk)
                self.bytes_uploaded = self.upload_wrapper.offset

    def finalize(self):
        if not self.success:
            return
        self.dbx_file_metadata = self.upload_wrapper.finalize()
        self._update_store()
        if self.channel_notify:
            self.provider.bus.publish(self.channel_notify, self.status())
        self.log('FINALIZED')

    def cleanup(self):
        pass

    def _update_store(self):
        self.provider.store.upsert(
            path=self.path,
            dbx_id=self.id,
            dbx_rev=self.rev,
            is_to_be_uploaded=False,
            whitelisted=True,
            last_synced=datetime.datetime.now()
        )

    def log(self, msg, traceback=False):
        prefix = \
            '[DROPBOX-UPLOAD] {path} (id:{id} rev:{rev}) {bytes_u}/{bytes_t}'\
            .format(path=self.path, id=self.id, rev=self.rev,
                    bytes_u=self.bytes_uploaded,
                    bytes_t=self.bytes_total or '??')
        msg = ' '.join((prefix, msg))
        super(DropboxUploadTask, self).log(msg, traceback)
