#!/usr/bin/env python2.7

"""
Generic download-related functionality.
"""

from __future__ import absolute_import

import abc
import cherrypy
import cherrypy.lib
from cloudcache.task import TaskBase
from cloudcache.service import WorkerPoolService


CHANNEL_DOWNLOAD = 'download'
CHANNEL_UPLOAD = 'upload'

class DownloadService(WorkerPoolService):
    """Queue and execute download tasks.

    :param n_workers: Number of worker threads to use for download task
                      execution.
    :param bus: Messaging bus to subscribe to.
    """

    _cp_skip_channels = ('log',)

    def __init__(self, n_workers, bus=cherrypy.engine):
        super(DownloadService, self).__init__(
            channel_enqueue=CHANNEL_DOWNLOAD,
            n_workers=n_workers,
            log_prefix='[DOWNLOAD-SERVICE]',
            bus=bus
        )


class UploadService(WorkerPoolService):
    """Queue and execute download tasks.

    :param n_workers: Number of worker threads to use for download task
                      execution.
    :param bus: Messaging bus to subscribe to.
    """

    _cp_skip_channels = ('log',)

    def __init__(self, n_workers, bus=cherrypy.engine):
        super(UploadService, self).__init__(
            channel_enqueue=CHANNEL_UPLOAD,
            n_workers=n_workers,
            log_prefix='[UPLOAD-SERVICE]',
            bus=bus
        )


class DownloadTaskBase(TaskBase):
    """Base class for download tasks. Describes download status and specifies
    how download should be handled.

    _start() and get_read_fileobj() methods should be implemented by
    inheriting classes.
    """

    __metaclass__ = abc.ABCMeta

    def __init__(self, path):
        self.path = path
        self.bytes_downloaded = 0
        self.bytes_total = None

        super(DownloadTaskBase, self).__init__()

    @abc.abstractmethod
    def _start(self):
        pass

    @abc.abstractmethod
    def get_read_fileobj(self, timeout):
        """Return a file-like object for reading  content from in-progress
        download.

        :param timeout: Time (in seconds) to wait for fileobj availability.
        """
        pass

    def status(self):
        status = super(DownloadTaskBase, self).status()
        status.update({
            'path': self.path,
            'bytes_downloaded': self.bytes_downloaded,
            'bytes_total': self.bytes_total,
        })
        return status


class UploadTaskBase(TaskBase):
    """Base class for download tasks. Describes download status and specifies
    how download should be handled.

    _start() and get_read_fileobj() methods should be implemented by
    inheriting classes.
    """

    __metaclass__ = abc.ABCMeta

    def __init__(self, path):
        self.path = path
        self.bytes_uploaded = 0
        self.bytes_total = None

        super(UploadTaskBase, self).__init__()

    @abc.abstractmethod
    def _start(self):
        pass

    def status(self):
        status = super(UploadTaskBase, self).status()
        status.update({
            'path': self.path,
            'bytes_uploaded': self.bytes_uploaded,
            'bytes_total': self.bytes_total,
        })
        return status
