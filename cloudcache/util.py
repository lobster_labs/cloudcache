#!/usr/bin/env python2.7

"""
Common utilities.
"""

import datetime
import errno
import json
import os
import pprint
import tempfile
import time


pretty_printer = pprint.PrettyPrinter()


class FromConfigMixin(object):
    @classmethod
    def from_config(cls, prefix, config, **additional_kwargs):
        """Return class instance initialized from config.

        :param prefix: Config key prefix.
        :param config: Config dictionary.
        :param kwargs: Additional parameters
        :type config: dict
        """
        kwargs = {}
        for k, v in config.iteritems():
            if k.startswith(prefix):
                k = (k.lstrip(prefix)
                      .lstrip('.'))
                if 'filepath' in k or 'dirpath' in k:
                    v = expanded_abspath(v)
                kwargs[k] = v
        kwargs.update(additional_kwargs)
        return cls(**kwargs)


class BusSubscribeMixin(object):
    """Provides a slightly modified methods for subscribing/unsubscribing to
    CherryPy bus.

    Based on equivalent functions from cherrypy.process.plugin.SimplePlugin.
    """
    def subscribe(self, priorities=None):
        """Register this object as a (multi-channel) listener on the bus."""
        skip_channels = getattr(self, '_cp_skip_channels', ())
        priorities = priorities if priorities is not None else {}

        for channel in self.bus.listeners:
            # Subscribe self.start, self.exit, etc. if present.
            if channel in skip_channels:
                continue
            method = getattr(self, channel, None)
            priority = priorities.get(channel, None)
            if method is not None:
                self.bus.subscribe(channel, method, priority)


class LiveFileReader(object):
    """File-like object for handling reading from written-to files.

    :param file_writer: File-like object used for writing.
    :param expected_size: Expected size of written-to file in bytes.
    """

    def __init__(self, file_writer, expected_size):
        self.file_writer = file_writer
        self.file_reader = open(file_writer.name, 'rb')
        self.expected_size = expected_size
        self.wait_interval = 0.2

    def __getattr__(self, attrname):
        """Expose attributes from underlying file object."""
        exposed_attr = ('close', 'closed', 'fileno', 'mode', 'name', 'tell')
        if attrname in exposed_attr:
            return getattr(self.file_reader, attrname)
        else:
            raise AttributeError(attrname)

    def __enter__(self):
        if self.closed:
            raise ValueError("Cannot enter context with closed file")
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    @property
    def size(self):
        """Return largest possible file offset."""
        offset = self.file_reader.tell()
        self.file_reader.seek(0, os.SEEK_END)
        size = self.file_reader.tell()
        self.file_reader.seek(offset)
        return size

    def read(self, size=None):
        left = min((size, self.expected_size - self.tell()))
        buf = self.file_reader.read(left)
        left -= len(buf)

        # Ensure we won't return 0 bytes prematurely
        while left > 0:
            time.sleep(self.wait_interval)
            buf += self.file_reader.read(left)
            left -= len(buf)

            # Don't loop if writer is closed.
            if self.file_writer.closed:
                break

        return buf

    def seek(self, offset, whence=os.SEEK_SET):
        if whence != os.SEEK_SET:
            raise ValueError(
                'Only offsets from beginning of the file are accepted.'
            )
        if whence == os.SEEK_CUR:
            offset += self.tell()

        if offset > self.expected_size:
            raise ValueError(
                'Offset greater than expected file size: %s > %s'
                % (offset, self.expected_size)
            )

        while offset < self.size:
            break

        self.file_reader.seek(offset)


class CacheFile(object):
    """Creates a writeable cache file with unique name. Works like
    tempfile.NamedTemporaryFile, but removes file only on failed operation.

    :param cache_dirpath: Destination cache dir.
    """
    def __init__(self, cache_dirpath):
        makedirs(cache_dirpath, exist_ok=True)
        self.file = tempfile.NamedTemporaryFile(
            mode='wb',
            prefix='cache_',
            dir=cache_dirpath,
            delete=False
        )

    def __getattr__(self, name):
        """Expose attributes of underlying file object."""
        return getattr(self.file, name)

    def close_fail(self):
        """Close and remove the file."""
        self.close()
        os.unlink(self.name)

    def __enter__(self):
        if self.closed:
            raise ValueError("Cannot enter context with closed file")
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is None:
            self.close()
        else:
            self.close_fail()


class DummyFile(object):
    def __init__(self, *args, **kwargs):
        self.size = kwargs.pop('size')
        self.bytes_read = 0

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        return

    def close(self):
        pass

    def write(self, buf):
        pass

    def read(self, n=None):
        n = n if n is not None else self.size
        to_read = min((self.size - self.bytes_read), n)
        self.bytes_read += to_read
        return '0' * to_read


def makedirs(path, mode=0777, exist_ok=False):
    try:
        os.makedirs(path)
    except OSError as err:
        if exist_ok and err.errno == errno.EEXIST:
            pass
        else:
            raise


def expanded_abspath(path):
    """Return absolute path with expanded ~ and shell variables."""
    path = os.path.expandvars(path)
    path = os.path.expanduser(path)
    path = os.path.abspath(path)
    return path


class JSONEncoder(json.JSONEncoder):
    """Serialize objects to JSON.

    Datetime object are converted to ISO 8601 format with 'T' separator.
    Miliseconds are stripped.
    """
    def default(self, o):
        to_json = getattr(o, 'to_json', None)
        if to_json:
            return to_json()
        elif isinstance(o, datetime.datetime):
            return o.replace(microsecond=0).isoformat()
        elif isinstance(o, Exception):
            return str(o)
        else:
            return json.JSONEncoder.default(self, o)

json_encoder = JSONEncoder()
