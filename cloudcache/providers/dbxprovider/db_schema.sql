CREATE TABLE IF NOT EXISTS file (
    id                  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    path                VARCHAR UNIQUE NOT NULL,
    cache_filepath      VARCHAR,
    thumb_filepath      VARCHAR,
    is_dir              BOOLEAN DEFAULT 0,
    is_whitelisted      BOOLEAN DEFAULT 0,
    is_to_be_uploaded   BOOLEAN DEFAULT 0,
    dbx_id              VARCHAR,
    dbx_rev             VARCHAR,
    last_synced         TIMESTAMP
);
