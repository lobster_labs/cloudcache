#!/usr/bin/env python2.7

"""
Web APIs.
"""


from __future__ import absolute_import

import cherrypy
import cherrypy.lib
import cherrypy.lib.static
import cherrypy.process.plugins
import functools
from cloudcache.util import json_encoder


def large_upload():
    cherrypy.response.timeout = 3600
    cherrypy.request.process_request_body = False

cherrypy.tools.large_upload = cherrypy.Tool('before_request_body', large_upload)


def error_handler(f):
    """Translate exceptions to HTTP response with appropriate status code
    and JSON-serialized response body.

    Exceptions can specify HTTP status code via http_status_code attribute, with
    HTTP status 500 (Internal Server Error) as the default.
    """
    @functools.wraps(f)
    def wrapped(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as err:
            status_code = getattr(err, 'http_status_code', 500)
            cherrypy.log('An exception occured', traceback=True)

            response = cherrypy.serving.response
            response.status = status_code
            response.headers['Content-Type'] = 'application/json'

            return json_encoder.encode(
                {
                    'error': {
                        'type': type(err).__name__,
                        'args': err.args
                    }
                }
            )
    return wrapped


def json_handler(*args, **kwargs):
    """CherryPy request handler using custom JSON encoder."""
    value = cherrypy.serving.request._json_inner_handler(*args, **kwargs)
    return json_encoder.iterencode(value)


def str_to_bool(o):
    return str(o).lower() in ('true', 'yes', 'aye', 'yup', 'totally', 'yessir')


class DropboxAPI(object):
    """Web API for dropbox provider."""

    def __init__(self, dbx_provider=None):
        self.dbx_provider = dbx_provider

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @cherrypy.tools.json_out(handler=json_handler)
    @error_handler
    def status(self):
        """Return storage status from storage provider."""
        return self.dbx_provider.status()

    @cherrypy.expose
    @error_handler
    def index(self):
        status = self.dbx_provider.status()
        result = ['<HTML>']
        for path, details in status['files'].iteritems():
            if details['is_dir']:
                continue

            result.append('<b>{}</b>'.format(path))
            result.append(
                '<i>l:{:.2f}MB/r:{:.2f}MB</i>'.format(
                    (details.get('local_size') or 0) / 1024.0**2,
                    (details.get('dbx_size') or 0) / 1024.0**2
                )
            )

            if details['in_cache']:
                result.append(
                    '<a href="fetch?path={}">IN CACHE</a>'.format(path)
                )
            if details['in_cloud']:
                result.append(
                    '<a href="fetch?ignore_cache=true&path={}">IN CLOUD</a>'.format(path)
                )

            if details['whitelisted']:
                result.append(
                    '<a href="blacklist?path={}">BLKLST</a>'.format(path)
                )
            else:
                result.append(
                    '<a href="whitelist?path={}">WHTLST</a>'.format(path)
                )
            result.append('<br><br>')
        result.append('</HTML>')

        return '\n'.join(result)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @error_handler
    def fetch(self, path, ignore_cache='false'):
        """Request a file from storage."""
        cherrypy.serving.response.stream = True

        ignore_cache = str_to_bool(ignore_cache)

        if ignore_cache:
            file_metadata, http_response = self.dbx_provider.download(path)
            return serve_from_response(
                http_response,
                name=file_metadata.name
            )
        else:
            file_metadata, fileobj = self.dbx_provider.get_read_fileobj(path)
            return serve_fileobj(
                fileobj,
                content_length=file_metadata.size,
                name=file_metadata.name
            )

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @error_handler
    def fetch_thumbnail(self, path):
        """Request thumbnail from local storage."""
        file_metadata, thumbnailfileobj = self.dbx_provider.get_thumbnail_read_fileobj(path)
        return serve_fileobj(
            thumbnailfileobj,
            content_length=thumbnailfileobj.tell(),
            name='thumbnail_{}'.format(file_metadata.name)
        )

    @cherrypy.expose
    @cherrypy.tools.large_upload()
    @cherrypy.tools.allow(methods=('POST',))
    @error_handler
    def push(self, path, ignore_cache='false'):
        """Push a file to storage."""
        fileobj = cherrypy.request.body.fp
        ignore_cache = str_to_bool(ignore_cache)
        if ignore_cache:
            self.dbx_provider.upload_direct(fileobj, path)
        else:
            self.dbx_provider.receive_upload(fileobj, path)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @error_handler
    def whitelist(self, path):
        """Whitelist file for sync."""
        self.dbx_provider.update_whitelisted(path, True)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @error_handler
    def blacklist(self, path):
        """Blacklist file from sync."""
        self.dbx_provider.update_whitelisted(path, False)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @error_handler
    def remove(self, path):
        """Remove a file permanently"""
        self.dbx_provider.remove_cached_file(path)
        self.dbx_provider.remove_remote_file(path)

    @cherrypy.expose
    @error_handler
    def upload_form(self):
        """Return static form for file upload"""
        return open('cloudcache/templates/upload_file.html')


class NotificationServiceAPI(object):
    def __init__(self, notification_service):
        self.notification_service = notification_service

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('POST',))
    @error_handler
    def register(self, token):
        """Register new message recipient."""
        self.notification_service.register(token)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('POST',))
    @error_handler
    def unregister(self, token):
        """Unregister an existing message recipient. Raise exception if
        token is not registered."""
        self.notification_service.unregister(token)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=('GET',))
    @cherrypy.tools.json_out(handler=json_handler)
    @error_handler
    def is_registered(self, token):
        """Return True if token is registered."""
        return {
            'result': self.notification_service.is_registered(token)
        }


def serve_fileobj(fileobj, content_length, name=None,
                  content_type='application/octet-stream'):
    """Serve content from file-like object."""
    if name:
        cherrypy.response.headers['Content-Disposition'] = \
            'attachment; filename="%s"' % name

    return cherrypy.lib.static._serve_fileobj(
        fileobj=fileobj,
        content_type=content_type,
        content_length=content_length,
        debug=True
    )


def serve_from_response(response, name=None, chunk_size=64*1024):
    """Serve content from HTTP response."""
    copy_headers = ('Content-Length', 'Content-Type', 'Content-Disposition')
    cherrypy.response.headers.update(
        {h_name: response.headers.get(h_name) for h_name in copy_headers}
    )

    if name:
        cherrypy.response.headers['Content-Disposition'] = \
            'attachment; filename="%s"' % name

    return response.iter_content(chunk_size)
