#!/usr/bin/env python2.7

from __future__ import absolute_import

from cloudcache.task import CancelledException


class DropboxProviderException(Exception):
    pass


class DropboxFileNotFoundException(DropboxProviderException):
    http_status_code = 404


class DropboxThumbnailNotExists(DropboxProviderException):
    pass
