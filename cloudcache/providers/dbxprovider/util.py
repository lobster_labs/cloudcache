#!/usr/bin/env python2.7


from __future__ import absolute_import

from . import exceptions
import dropbox
import dropbox.files


class DropboxUploadWrapper(object):
    """Wrap Dropbox multi-part upload in a simpler interface.

    send() method sends content chunks to Dropbox. Calling finalize() finishes
    the upload.

    Single chunks shouldn't be bigger than 150MB, as per Dropbox's API
    recommendation.

    :param provider: DropboxProvider instance.
    :param path: Upload destination path, relative to root filepath. Should
                start with '/'.
    """

    MAX_CHUNK_SIZE = 150 * 1024 ** 2  # 150 MB

    def __init__(self, provider, path,
                 commit_mode=dropbox.files.WriteMode('overwrite')):
        self.provider = provider
        self.path = path
        self.commit_mode = commit_mode
        self.offset = 0
        self.session_id = None

    def send(self, chunk):
        try:
            self._send(chunk)
        except Exception as err:
            raise exceptions.DropboxProviderException(err)

    def _send(self, chunk):
        chunk_size = len(chunk) if chunk is not None else 0

        if chunk_size > self.MAX_CHUNK_SIZE:
            raise ValueError(
                'Chunk size too big: %s > %s'
                % (chunk_size, self.MAX_CHUNK_SIZE)
            )

        if self.session_id is None:  # Start new upload session
            session = self.provider.client.files_upload_session_start(f=chunk)
            self.session_id = session.session_id

        else:  # Append to existing upload session
            self.provider.client.files_upload_session_append(
                f=chunk,
                session_id=self.session_id,
                offset=self.offset
            )

        self.offset += chunk_size

    def finalize(self):
        commit = dropbox.files.CommitInfo(
            path=self.path,
            mode=self.commit_mode
        )
        cursor = dropbox.files.UploadSessionCursor(
            session_id=self.session_id,
            offset=self.offset
        )
        file_metadata = self.provider.client.files_upload_session_finish(
            f='',
            cursor=cursor,
            commit=commit
        )
        return file_metadata


def group_by_path_lower(iterable):
    """Return itrable contents keyed by their 'path_lower' attribute."""
    return group_iterable_by_attribute(
        iterable,
        attr_getter=lambda x: x.path_lower,
    )


def group_iterable_by_attribute(iterable, attr_getter, many=False):
    """Return dictionary with iterable items grouped by attribute
    dictated by attr_getter function.

    :param attr_getter: Single-argument function that returns a hashable object
    to use as dictionary key.
    :type attr_getter: function
    :param many: If True, items will be aggregated into a list.

    :rtype dict
    """
    result = {}
    for item in iterable:
        key = attr_getter(item)
        if many:
            result.setdefault(key, []).append(item)
        else:
            result[key] = item
    return result
